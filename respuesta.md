# Práctica 5. Sesión SIP

## Ejercicio 3
### ¿Cuántos paquetes componen la captura?
1050 paquetes
### ¿Cuánto tiempo dura la captura?
10.52s
### ¿Qué IP tiene la máquina donde se ha efectuado la captura?
192.168.1.116
### ¿Se trata de una IP pública o de una IP privada?
Privada
### ¿Por qué lo sabes?
Porque está en el rango de IPs privada (192.168.0.0 / 192.168.255.255)
### ¿Cuáles son los tres principales protocolos de nivel de aplicación por número de paquetes?
UDP, SIP y RTP
### ¿Qué otros protocolos podemos ver en la jerarquía de protocolos?
Ethernet, IP y ICMP
### ¿Qué protocolo de nivel de aplicación presenta más tráfico, en bytes por segundo? ¿Cuánto es ese tráfico?
RTP (134 Kbit/s)
### ¿En qué segundos tienen lugar los dos primeros envíos SIP?
Segundo 0
### Y los paquetes con RTP, ¿en qué paquete de la trama se empiezan a enviar?
Del 6 hacia adelante
### Los paquetes RTP, ¿cada cuánto se envían?
Cada 0.01s

## Ejercicio 4
### ¿De qué protocolo de nivel de aplicación son?
SIP
### ¿Cuál es la dirección IP de la máquina "Linphone"?
192.168.1.116
### ¿Cuál es la dirección IP de la máquina "Servidor"?
212.79.111.155
### ¿Que ha ocurrido (en Linphone o en Servidor) para que se envíe la primera trama?
Se envía un request (Linphone) para conectar con el Servidor
### ¿Qué ha ocurrido (en Linphone o en Servidor) para que se envíe la segunda trama?
Se envía un trying (Servidor) a Linphone
### ¿De qué protocolo de nivel de aplicación son?
SIP
### ¿Entre qué máquinas se envía cada trama?
Linphone y Servidor
### ¿Que ha ocurrido para que se envíe la primera de ellas (tercera trama en la captura)?
Servidor manda un OK
### ¿Qué ha ocurrido para que se envíe la segunda de ellas (cuarta trama en la captura)?
Linphone manda el ACK

## Ejercicio 5
### ¿Qué número de trama es?
1042
### ¿De qué máquina a qué máquina va?
Linphone a Servidor
### ¿Para qué sirve?
Poner fin a la comunicación
### ¿Puedes localizar en ella qué versión de Linphone se está usando?
Linphone Desktop/ 4.3.2

## Ejercicio 6
### ¿Cuál es la dirección SIP con la que se quiere establecer una llamada?
music@sip.iptel.org
### ¿Qué instrucciones SIP entiende el UA?
INVITE, ACK, CANCEL, OPTIONS, BYE, REFER,  NOTIFY, MESSAGE, SUBSCRIBE, INFO, PRACK y UPDATE
### ¿Qué cabecera SIP indica que la información de sesión va en formato SDP?
content-type.
### ¿Cuál es el nombre de la sesión SIP?
Talk

## Ejercicio 7
### ¿Qué trama lleva esta propuesta?
La segunda
### ¿Qué indica el 7078?
La dirección de transporte
### ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
El puerto por el que se envían o reciben los paquetes
### ¿Qué paquetes son esos?
RTP
### ¿Qué trama lleva esta respuesta?
La cuarta
### ¿Qué valor es el XXX?
29448
### ¿Qué relación tiene con el destino de algunos paquetes que veremos más adelante en la trama?
El puerto por el que se reciben o envían los paquetes
### ¿Qué paquetes son esos?
RTP

## Ejercicio 8
### ¿De qué máquina a qué máquina va?
Linphone al Servidor
### ¿Qué tipo de datos transporta?
Multimedia
### ¿Qué tamaño tiene?
214 bytes
### ¿Cuántos bits van en la "carga de pago" (payload)?
1280 bits (160 bytes)
### ¿Cuál es la periodicidad de los paquetes según salen de la máquina origen?
0.01s

## Ejercicio 9
### ¿Cuántos flujos hay? ¿por qué?
2, son las dos direcciones entre Linphone y Servidor
### ¿Cuántos paquetes se pierden?
Ninguno
### Para el flujo desde LinPhone hacia Servidor: ¿cuál es el valor máximo del delta?
30.73ms
### ¿Qué es lo que significa el valor de delta?
El tiempo entre envio de paquetes
### ¿En qué flujo son mayores los valores de jitter (medio y máximo)?
Servidor a Linphone
### ¿Qué significan esos valores?
Variación entre los distintos retardos
### ¿Cuánto valen el delta y el jitter para ese paquete?
Delta, 0.000164 y jitter, 3.087182
### ¿Podemos saber si hay algún paquete de este flujo, anterior a este, que aún no ha llegado?
Sí miramos el valor SKEW
### El "skew" es negativo, ¿qué quiere decir eso?
Que llega antes de tiempo
### ¿Qué se oye al pulsar play?
Una melodía
### ¿Qué se oye si seleccionas un Jitter Buffer de 1, y pulsas play?
Se oye entrecortado
### ¿A qué se debe la diferencia?
El valor de jitter es demasiado bajo, no se espera lo suficiente para que lleguen los paquetes

## Ejercicio 10
### ¿Cuánto dura la llamada?
10s
### ¿En qué segundo se recibe el último OK que marca el final de la llamada?
Segundo 10.52
### ¿Cuáles son las SSRC que intervienen?
Linphone a Server: 0x0d2db8b4, Server a Linphone: 0x5c44a34b
### ¿Cuántos paquetes se envían desde LinPhone hasta Servidor?
514
### ¿Cuál es la frecuencia de muestreo del audio?
8 KHz
### ¿Qué formato se usa para los paquetes de audio (payload)?
g711U

## Ejercicio 11
### ¿Cuántos flujos RTP tiene esa captura?
2
### Cuánto es el valor máximo del delta y los valores medios y máximo del jitter de acda uno de los flujos?
Flujo 1: Max Delta-> 41.96ms Mean Jitter -> 6.58ms Max Jitter-> 10.09ms
Flujo 2: Max Delta-> 31.02ms Mean Jitter -> 0.98ms Max Jitter-> 3.38ms